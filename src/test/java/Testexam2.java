import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class Testexam2 {

	int a;
	boolean result;
	@Parameters
	public static Collection<Object[]> numbers() {return Arrays.asList(new Object[][] {
		{0,true},
		{1,true},
		{4,false},
		{9,true},
		{10,false}
});
	}
	public Testexam2(int a, boolean result) {
		this.a = a;
		this.result = result;
	}
	
	@Test
	public void test() {
		exam2 exam2 = new exam2();
		assertEquals(result, exam2.exam(a));
	}

}
